/*
*	Custom Template
*
*/

#define MAX_SIZE 100

template< typename T > class arrayBuffer 
{

public:

	arrayBuffer() {}
	~arrayBuffer() {}
	arrayBuffer& operator[](size_t index)
	{
		return m_array[index];
	}

private:

	T m_array[MAX_SIZE];

};

arrayBuffer<int> intBuffer;
arrayBuffer<unsigned char> ucharBuffer;

int main()
{
	return 1;
}